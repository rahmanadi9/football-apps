package cb.arc.com.footballclub

data class TeamResponse (
    val teams: List<Team>)