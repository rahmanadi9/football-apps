package cb.arc.com.footballclub

import com.google.gson.Gson
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainPresenter (private val view: MainView,
                     private val apiRepository: ApiRepository,
                     private val gson: Gson){

    fun getTeamList(league: String?){
        view.showLoading()
        doAsync {
            val data = gson.fromJson(apiRepository
                .doRequest(TheSportDBApi.getTeams(league)),
                TeamResponse::class.java)

            uiThread {
                view.hideLoading()
                if(data != null){
                    view.showTeamList(data.teams)
                }else{

                }
            }
        }
    }
}